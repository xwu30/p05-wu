package com.example.xianwu.ninja;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class Ninja extends AppCompatActivity {
    TextView tv;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ninja);
//        TextView view1 = new TextView(this);
//        view1.setText("this is a try");
//        TextView view2 = new TextView(this);
//        view1.setText("this is a copy");
//        LinearLayout lay = new LinearLayout(this);
//        lay.addView(view1);
//        lay.addView(view2);
//        setContentView(lay);
        tv = (TextView)findViewById(R.id.tv1);
        //tv.setText("nimabi");
        tv.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/Pacifico.ttf"));
        //!!!!rememeber this to modify the font!
        btn = (Button)findViewById(R.id.btn1);
        btn.setOnClickListener(new View.OnClickListener() {
            public int cc = 0;
            @Override
            public void onClick(View v) {
                cc++;
                tv.setTextColor(Color.YELLOW);
                if(cc%2==1)
                tv.setText("LET'S NINJA!");
                if(cc%2==0)
                    tv.setText("Why do you click me again?just wait boy");
                Intent intent = new Intent(Ninja.this,Gamesence.class);
                startActivity(intent);
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ninja, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
