package com.example.xianwu.ninja;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import java.util.*;

public class MyView extends SurfaceView implements SurfaceHolder.Callback {
    private boolean flag = false;
    private SurfaceHolder surfaceHolder;
    private Bitmap bitmap,bitmap1,bmp,bmp1,bmp2;
    private int countb = 0;
    private boolean countb1 = true;
    private int xPos;
    private int yPos;
    private Random rand = new Random();
    private RotateAnimation rotateAnimation;

    private DisplayMetrics metrics;

    private int xVel;
    private int yVel;
    private float lastX, lastY;

    private int width,wid;
    private int height,hei;

    private int circleRadius;
    private Paint circlePaint,pback;
    public ImageView bbi;


    private Paint mPaint_weapon = null; // 真实的画笔
    private Path mPath_weapon = null; //
    // private Point fly_middle;
    private ArrayList<PointF> weapon_Track;
    private int weapon_Track_w = 3; // 这里设置刀的宽度哦，想刀宽一点就把这个值调大一点，不要太大哦
    private int weapon_color_r = 0xff, weapon_color_g = 0x00, weapon_color_b = 0x00;


    UpdateThread updateThread;

    public MyView(Context context) {
        super(context);
        surfaceHolder = this.getHolder();
        rotateAnimation = new RotateAnimation(0.0f,720.0f, Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        rotateAnimation.setDuration(14000);
        metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.watermelon1);
        bmp1 = BitmapFactory.decodeResource(getResources(), R.drawable.watermelon2);
        bmp2 = BitmapFactory.decodeResource(getResources(), R.drawable.watermelon2);
        wid = bmp.getWidth();
        hei = bmp.getHeight();
        bbi = new ImageView(context);
        bbi.setBackground(Drawable.createFromPath("drawable/peach"));
        bbi.layout(wid, hei, xPos, yPos);
        bbi.bringToFront();
        bbi.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                bmp = BitmapFactory.decodeResource(getResources(), R.drawable.passionfruit);
                return false;
            }
        });

//        bbi.setImageBitmap(bmp);
//        bbi.startAnimation(rotateAnimation);
//        this.startAnimation(rotateAnimation);

        bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.back2);
        bitmap = Bitmap.createScaledBitmap(bitmap,width,height,true);
        bitmap1 = BitmapFactory.decodeResource(getResources(),R.drawable.back3);
        bitmap1 = Bitmap.createScaledBitmap(bitmap1,width,height,true);
//        bbi = new ImageView(context);
//        bbi.setBackground(Drawable.createFromPath("drawable/back1"));
//        bbi.setScaleType(ImageView.ScaleType.FIT_XY);
        pback = new Paint();
        pback.setColor(Color.YELLOW);
        this.setBackground(Drawable.createFromPath("drawable/watermelon1"));
        getHolder().addCallback(this);

        circleRadius = 57;
        circlePaint = new Paint();
        circlePaint.setColor(Color.BLUE);

        xVel = 2;
        if(xPos>(width/2))xVel*=-1;
        yVel = -6;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        Canvas c = null;
//        int index = event.getActionIndex();
//        lastX = event.getX(index);
//        lastY = event.getY(index);
//        System.out.println((lastX>xPos)&&(lastX<(xPos+bmp.getWidth())) && (lastY > yPos) && (lastY < bmp.getHeight()));
//        System.out.println("1111111111xpos: "+xPos+"ypos: "+yPos+"xwidht: "+lastX+"hiehgt: "+lastY);
//        if((lastX>xPos)&&(lastX<(xPos+bmp.getWidth()))&&(lastY>yPos)&&(lastY<yPos+bmp.getHeight())){
//            Log.e("text","let's see hahahaha1212344241234123412341234132");
//            try {
//                c = surfaceHolder.lockCanvas(null);
//
//                this.updatePhysics();
//                onDraw(c, 1);
//            } finally {
//                if (c != null) {
//                    surfaceHolder.unlockCanvasAndPost(c);
//                }
//            }
//        }
        if (action == MotionEvent.ACTION_DOWN) {
            int index = event.getActionIndex();
            lastX = event.getX(index);
            lastY = event.getY(index);
            if((lastX>xPos)&&(lastX<(xPos+bmp.getWidth()))&&(lastY>yPos)&&(lastY<yPos+bmp.getHeight())){
                yVel*=-1;
//                Log.e("text","let's see hahahaha1212344241234123412341234132");
                try {
                    c = surfaceHolder.lockCanvas(null);

                    this.updatePhysics();
                    onDraw(c, 1);
                } finally {
                    if (c != null) {
                        surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }

//            this.postInvalidate();
        }
        return true;
    }
    protected void onDraw(Canvas canvas,int type) {

        //canvas.drawColor(Color.WHITE);
        //bitmap = Bitmap.creates;
        if(flag)type = 1;
        if(type == 0) {
            countb++;
            if (countb % 100 == 0) {
                countb1 = !countb1;
            }
            if (countb1)
                canvas.drawBitmap(bitmap, 0, 0, null);
            else canvas.drawBitmap(bitmap1, 0, 0, null);
//        canvas.drawCircle(xPos, yPos, circleRadius, circlePaint);
            canvas.drawBitmap(bmp, xPos, yPos, null);
            bbi.draw(canvas);
            bbi.bringToFront();
        }else if(type == 1) {
            flag = true;
            countb++;
            if (countb % 100 == 0) {
                countb1 = !countb1;
            }
            if (countb1)
                canvas.drawBitmap(bitmap, 0, 0, null);
            else canvas.drawBitmap(bitmap1, 0, 0, null);
//        canvas.drawCircle(xPos, yPos, circleRadius, circlePaint);
            canvas.drawBitmap(bmp1, xPos-50, yPos-20, null);
            canvas.drawBitmap(bmp2, xPos+50, yPos+20, null);
        }

    }
    public void updatePhysics() {

        //更新当前的x,y坐标
        xPos += xVel;
        yPos += yVel;

        if (yPos - circleRadius < 0 || yPos + circleRadius > height) {


            if (yPos - circleRadius < 0) {

                //如果小球到达画布区域的上顶端，则弹回

                yPos = circleRadius;
            }else{

                //如果小球到达了画布的下端边界，则弹回
                flag = false;
                this.surfaceCreated(this.surfaceHolder);
                yPos = height - circleRadius;
            }

            // 将Y坐标设置为相反方向
            yVel *= -1;
        }
        if (xPos  < 0 || xPos > width) {


            if (xPos < 0) {

                // 如果小球到达左边缘
                flag = false;
                this.surfaceCreated(this.surfaceHolder);
                xPos = circleRadius;
            } else {

                // 如果小球到达右边缘
                flag = false;
                this.surfaceCreated(this.surfaceHolder);
                xPos = width - circleRadius;
            }

            // 重新设置x轴坐标
            xVel *= -1;
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {

        Rect surfaceFrame = holder.getSurfaceFrame();
        width = surfaceFrame.width();
        height = surfaceFrame.height();

//        xPos = width / 2;
        xPos = rand.nextInt(width);

        yPos = height-circleRadius-10;
        if (xPos - circleRadius < 0 || xPos + circleRadius > width) {


            if (xPos - circleRadius < 0) {
                xPos = circleRadius;
            } else {
                xPos = width - circleRadius;
            }
        }


        updateThread = new UpdateThread(this);
        updateThread.setRunning(true);
        updateThread.start();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {

    }

    public void surfaceDestroyed(SurfaceHolder holder) {

        boolean retry = true;

        updateThread.setRunning(false);
        while (retry) {
            try {
                updateThread.join();
                retry = false;
            } catch (InterruptedException e) {

            }
        }
    }
}